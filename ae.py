import keras
import keras.backend as K
from keras.models import Sequential, Model
from keras.layers import Dense, Activation, Dropout, Input
from keras.initializers import random_normal, he_normal
from matplotlib import pyplot
import numpy as np
import os


def rmse(y_true, y_pred):
    err = K.sqrt(K.mean(K.sum(K.square(y_pred - y_true), axis=1)))  # RMSE
    if ae_dict['contractive']:
        ind = len(ae_dict['hidden_layers']) / 2 + 1
        W = K.variable(value=model.get_layer(index=ind).get_weights()[0])  # N x N_hidden
        W = K.transpose(W)  # N_hidden x N
        h = model.get_layer(index=ind).output
        dh = h * (1 - h)  # N_batch x N_hidden

        # N_batch x N_hidden * N_hidden x 1 = N_batch x 1
        err += ae_dict['lamda'] * K.sum(dh ** 2 * K.sum(W ** 2, axis=1), axis=1)
    return err


# Feature Options:
# singleframe acoustic: 39
# multiframe acoustic: 351
# singleframe articulatory: 36
# multiframe articulatory: TODO

# Label Options:
# denormalized
# normalized

# Options dictionaries
data_dict = {
    'feature_frame': 'multiframe',
    'label_frame': 'singleframe',
    'label_option': 'normalized'
}

if data_dict['feature_frame'] == 'singleframe':
    input_dimension = 39
else:
    input_dimension = 39 * 9
if data_dict['label_frame'] == 'singleframe':
    output_dimension = 36
else:
    output_dimension = None  # TODO

ae_dict = {
    'input_dim': 410,  # input_dimension,
    'output_dim': 410,  # output_dimension,
    'hidden_layers': [300, 300, 12, 300, 300],
    'epochs': 20,
    'batch_size': 128,
    'learning_rate': 0.01,
    'optimizer': 'Adam',  # SGD, Adam, RMSProp
    'L2_regularization': 0.0,
    'activation': ['sigmoid', 'sigmoid', 'sigmoid', 'sigmoid', 'sigmoid', 'linear'],
    'dropout': 0.0,
    'batch_normalization': True,
    'lamda': 0.0001,
    'contractive': True
}

params_dict = {
    'feat_dir': '/home/cpalyvos/MNGU0/',
    # 'feat_dir': '/media/nick/Files-Linux/Datasets/speech_datasets/MNGU0/plp/' + data_dict['feature_frame'],
    'lab_dir': '/home/cpalyvos/MNGU0',
    # 'lab_dir': '/media/nick/Files-Linux/Datasets/speech_datasets/MNGU0/ema/articulatory_' + data_dict['label_option'],
    'work_dir': '/home/cpalyvos/final/'
}

# Keras Model
model = Sequential()
# Input layer
model.add(Dense(units=ae_dict['hidden_layers'][0],
                input_dim=ae_dict['input_dim'],
                activation=ae_dict['activation'][0],
                kernel_initializer=he_normal()
                ))
model.add(Dropout(rate=ae_dict['dropout']))
# Hidden layers
for i in range(len(ae_dict['hidden_layers']) - 1):
    model.add(Dense(units=ae_dict['hidden_layers'][i + 1],
                    activation=ae_dict['activation'][i + 1],
                    kernel_initializer=he_normal(),
                    ))
    model.add(Dropout(rate=ae_dict['dropout']))

# Output layer
model.add(Dense(units=ae_dict['output_dim'],
                activation=ae_dict['activation'][-1]
                ))

# Loss
model.compile(optimizer=ae_dict['optimizer'],
              loss=rmse,
              metrics=[rmse])

print(model.summary())

x_train = np.loadtxt(os.path.join(params_dict['feat_dir'], 'train.txt'), dtype='float32')
# y_train = np.loadtxt(os.path.join(params_dict['lab_dir'], 'train_labels.txt'), dtype='float32')

x_valid = np.loadtxt(os.path.join(params_dict['feat_dir'], 'valid.txt'), dtype='float32')
# y_valid = np.loadtxt(os.path.join(params_dict['lab_dir'], 'valid_labels.txt'), dtype='float32')

x_test = np.loadtxt(os.path.join(params_dict['feat_dir'], 'test.txt'), dtype='float32')
# y_test = np.loadtxt(os.path.join(params_dict['lab_dir'], 'test_labels.txt'), dtype='float32')

# Tensorboard
tbCallBack = keras.callbacks.TensorBoard(log_dir=os.path.join(params_dict['work_dir'], 'TensorboardAE'),
                                         histogram_freq=0, write_graph=True, write_images=True)
checkpointer = keras.callbacks.ModelCheckpoint(filepath=params_dict['work_dir'] + 'model_AE.hdf5', verbose=1,
                                               save_best_only=True)
# Train
history = model.fit(x=x_train,
                    y=x_train,
                    batch_size=ae_dict['batch_size'],
                    epochs=ae_dict['epochs'],
                    verbose=2,
                    validation_data=(x_valid, x_valid),
                    callbacks=[tbCallBack, checkpointer])
# Test
loss_and_metrics = model.evaluate(x_test, x_test, batch_size=ae_dict['batch_size'])
print('The RMSE on the test set is: ' + str(loss_and_metrics))

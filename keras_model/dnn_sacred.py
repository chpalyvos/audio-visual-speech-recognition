import keras
import keras.backend as K
from keras.models import Sequential, load_model
from keras.layers import Dense, Activation, Dropout, BatchNormalization
from keras.optimizers import SGD
from keras.initializers import he_normal
from keras.callbacks import TensorBoard, ModelCheckpoint
from config import Directories
import numpy as np
import os
from sacred import Experiment
from sacred.observers import MongoObserver
from sacred.stflow import LogFileWriter
from sklearn import preprocessing
from matplotlib import pyplot as plt
import cPickle


def rmse(y_true, y_pred):
    return K.sqrt(K.mean(K.sum(K.square(y_pred - y_true), axis=-1)))


ex = Experiment('Articulatory_Inversion_DNN')
ex.observers.append(MongoObserver.create(url=Directories.SacredIP,
                                         db_name=Directories.SacredDB))


@ex.config
def dnn_config():
    # Feature Options:
    # singleframe acoustic: 39
    # multiframe acoustic: 351
    # singleframe articulatory: 36
    # multiframe articulatory: TODO

    # Label Options:
    # denormalized
    # normalized

    feature_type = 'plp'  # plp, fbank, lsf
    feature_frames = 9
    feature_deltas = True
    label_deltas = False
    label_option = 'normalized'

    _feat_dim = 13 if feature_type == 'plp' else 23 if feature_type == 'fbank' else 41
    _multiplier = 3 if feature_deltas else 1

    input_dim = _feat_dim * _multiplier if feature_frames == 1 else _feat_dim * _multiplier * feature_frames
    output_dim = 36 if label_deltas else 12
    hidden_layers = [300, 300]
    epochs = 20
    batch_size = 256
    learning_rate = 0.01
    optimizer = 'SGD'  # SGD, Adam, RMSProp
    L2_regularization = 0.0
    activation = 'sigmoid'
    dropout = 0.6


@ex.automain
@LogFileWriter(ex)
def dnn_main(feature_type, feature_frames, feature_deltas, label_deltas, label_option,
             input_dim, output_dim, hidden_layers, epochs, batch_size, learning_rate, optimizer, L2_regularization,
             activation, dropout, _run):
    # Directories
    feature_splicing = 'multiframe' if feature_frames > 1 else 'singleframe'
    feat_dir = "{0}{1}/{2}_{3}{4}".format(Directories.Features,
                                          feature_type,
                                          feature_splicing,
                                          str(feature_frames),
                                          '_deltas' if feature_deltas else ''
                                          )
    lab_dir = "{0}{1}{2}".format(Directories.Labels,
                                 label_option,
                                 '_deltas' if label_deltas else ''
                                 )
    work_dir = os.path.join(Directories.Work, 'articulatory_inversion_dnn')
    # Work directory
    if not os.path.exists(work_dir):
        os.makedirs(work_dir)
        os.makedirs(os.path.join(work_dir, 'Tensorboard'))
    # Load data
    x_train = np.loadtxt(os.path.join(feat_dir, 'train.txt'), dtype='float32')
    y_train = np.loadtxt(os.path.join(lab_dir, 'ema_train.txt'), dtype='float32')

    x_valid = np.loadtxt(os.path.join(feat_dir, 'valid.txt'), dtype='float32')
    y_valid = np.loadtxt(os.path.join(lab_dir, 'ema_valid.txt'), dtype='float32')

    x_test = np.loadtxt(os.path.join(feat_dir, 'test.txt'), dtype='float32')
    y_test = np.loadtxt(os.path.join(lab_dir, 'ema_test.txt'), dtype='float32')

    # Normalize
    x_train = preprocessing.scale(x_train)
    x_valid = preprocessing.scale(x_valid)
    x_test = preprocessing.scale(x_test)

    # Keras Model
    model = Sequential()
    # Input layer
    model.add(Dense(units=hidden_layers[0], input_dim=input_dim))
    model.add(BatchNormalization())
    model.add(Activation(activation))
    model.add(Dropout(dropout))
    # Hidden layers
    for i in range(len(hidden_layers) - 1):
        model.add(Dense(units=hidden_layers[i + 1]))
        model.add(BatchNormalization())
        model.add(Activation(activation))
        model.add(Dropout(dropout))
    # Output layer
    model.add(Dense(units=output_dim))
    model.add(BatchNormalization())
    # Loss
    if optimizer == 'SGD':
        sgd = SGD(lr=learning_rate, decay=1e-6, momentum=0.9, nesterov=True)
        model.compile(optimizer=sgd,
                      loss=rmse)
    else:
        model.compile(optimizer=optimizer,
                      loss=rmse)
    # Callbacks
    _callbacks = []
    # Tensorboard
    tbcallback = TensorBoard(log_dir=os.path.join(work_dir, 'Tensorboard'), histogram_freq=0,
                             write_graph=True, write_images=True)
    # Checkpoint
    best_model = "{}".format('-'.join(['BEST_features-', feature_type, feature_splicing, str(feature_deltas),
                                       str(feature_frames), 'labels-', str(label_deltas), label_option, '.hdf5']))
    checkpointer = ModelCheckpoint(filepath=os.path.join(work_dir, best_model),
                                   monitor='val_loss', mode="min",
                                   verbose=1, save_best_only=True)

    _callbacks.append(tbcallback)
    # _callbacks.append(plotting)
    # _callbacks.append(weights)
    _callbacks.append(checkpointer)

    # Train
    history = model.fit(x=x_train,
                        y=y_train,
                        batch_size=batch_size,
                        epochs=epochs,
                        verbose=2,
                        validation_data=(x_valid, y_valid),
                        callbacks=_callbacks)
    for idx, loss in enumerate(history.history['val_loss']):
        _run.log_scalar("validation.loss", loss, idx)
    # Test
    test_loss = model.evaluate(x_test, y_test, batch_size=batch_size)
    print('The RMSE on the test set is: ' + str(test_loss))

    # Plot trajectories on best model
    model = load_model(os.path.join(work_dir, best_model), custom_objects={'rmse': rmse})
    reconstructed_labels = model.predict(x_test, batch_size=256, verbose=2)
    with open('groundtruth_articulatory_test_labels.pkl') as f:
        groundtruth = cPickle.load(f)
    sample_reconstructed = []
    for i in range(1000):
        sample_reconstructed.append(reconstructed_labels[i][0])
    sample_ground = []
    for i in range(1000):
        sample_ground.append(groundtruth[i][0])
    plt.plot(sample_reconstructed, 'red', label='Reconstructed')
    plt.plot(sample_ground, 'blue', label='Groundtruth')
    plt.legend()
    plt.show()

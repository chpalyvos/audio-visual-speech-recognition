import keras.backend as K
from keras.models import Sequential, load_model
from keras.layers import InputLayer, Dense, Activation, Dropout, BatchNormalization
from keras.optimizers import SGD
from keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping
from keras import regularizers
import numpy as np
import os
from matplotlib import pyplot as plt
from scripts.input_output.txt_io import TxtDataRead


def rmse(y_true, y_pred):
    return K.sqrt(K.mean(K.sum(K.square(y_pred - y_true), axis=-1)))


input_dim = 351
output_dim = 12
hidden_layers = [400, 400]
epochs = 30
batch_size = 256
learning_rate = 0.01
optimizer = 'Adam'  # SGD, Adam, RMSProp
L2_regularization = 0.0
activation = 'sigmoid'
dropout = 0.4

feat_dir = '/media/nick/Files-Linux/Datasets/speech_datasets/MNGU0/dnn_data'
lab_dir = feat_dir
work_dir = '/media/nick/Files-Linux/articulatory-inversion-keras/articulatory_inversion_plp'

# Load data
# x_train = np.loadtxt(os.path.join(feat_dir, 'train.txt'), dtype='float32')
# y_train = np.loadtxt(os.path.join(lab_dir, 'train_labels.txt'), dtype='float32')
#
# x_valid = np.loadtxt(os.path.join(feat_dir, 'valid.txt'), dtype='float32')
# y_valid = np.loadtxt(os.path.join(lab_dir, 'valid_labels.txt'), dtype='float32')


# Load data
x_train = os.path.join(feat_dir, 'train.txt')
y_train = os.path.join(lab_dir, 'train_labels.txt')

x_valid = os.path.join(feat_dir, 'valid.txt')
y_valid = os.path.join(lab_dir, 'valid_labels.txt')

# Load data readers
reader_train = TxtDataRead(feat_file_path=x_train, lab_file_path=y_train, batch_size=batch_size)
reader_val = TxtDataRead(feat_file_path=x_valid, lab_file_path=y_valid, batch_size=batch_size)
trSteps = 762754 // batch_size
valSteps = 38958 // batch_size

# Load test data
x_test = np.loadtxt(os.path.join(feat_dir, 'test.txt'), dtype='float32')
y_test = np.loadtxt(os.path.join(lab_dir, 'test_labels.txt'), dtype='float32')

# Keras Model
model = Sequential()
# Input layer
model.add(InputLayer(batch_input_shape=(None, input_dim), name='input'))
# Hidden layers
for i in range(len(hidden_layers)):
    model.add(Dense(units=hidden_layers[i],
                    # kernel_initializer='he_normal',
                    kernel_regularizer=regularizers.l2(L2_regularization),
                    name='hidden{}'.format(str(i))))
    model.add(BatchNormalization())
    model.add(Activation(activation))
    model.add(Dropout(dropout))
# Output layer
model.add(Dense(units=output_dim, name='encoded'))
model.add(BatchNormalization())

# Loss
if optimizer == 'SGD':
    sgd = SGD(lr=learning_rate, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(optimizer=sgd,
                  loss=rmse)
else:
    model.compile(optimizer=optimizer,
                  loss=rmse)
# Callbacks
_callbacks = []
# Tensorboard
tbcallback = TensorBoard(log_dir=os.path.join(work_dir, 'Tensorboard'), histogram_freq=0,
                         write_graph=True, write_images=True)
# Work directory
if not os.path.exists(work_dir):
    os.makedirs(work_dir)
    os.makedirs(os.path.join(work_dir, 'Tensorboard'))
# Checkpoint
best_model = "best.hdf5"
checkpointer = ModelCheckpoint(filepath=os.path.join(work_dir, best_model),
                               monitor='val_loss', mode="min",
                               verbose=1, save_best_only=True)
early_stop = EarlyStopping(monitor='val_loss',
                           min_delta=0,
                           patience=5, verbose=1, mode='min')

_callbacks.append(tbcallback)
# _callbacks.append(plotting)
# _callbacks.append(weights)
_callbacks.append(checkpointer)
_callbacks.append(early_stop)

# Train
# history = model.fit(x=x_train,
#                     y=y_train,
#                     batch_size=batch_size,
#                     epochs=epochs,
#                     verbose=2,
#                     validation_data=(x_valid, y_valid),
#                     callbacks=_callbacks)

history = model.fit_generator(reader_train.batch_generator(),
                              steps_per_epoch=trSteps,
                              validation_data=reader_val.batch_generator(),
                              validation_steps=valSteps,
                              epochs=epochs,
                              verbose=2,
                              callbacks=_callbacks)

# Test
test_loss = model.evaluate(x_test, y_test, batch_size=batch_size)
print('The RMSE on the test set is: ' + str(test_loss))

# Plot trajectories on best model
model = load_model(os.path.join(work_dir, best_model), custom_objects={'rmse': rmse})
reconstructed_labels = model.predict(x_test)
sample_reconstructed = []
for i in range(1000):
    sample_reconstructed.append(reconstructed_labels[i][0])
sample_ground = []
for i in range(1000):
    sample_ground.append(y_test[i][0])
plt.plot(sample_reconstructed, 'red', label='Reconstructed')
plt.plot(sample_ground, 'blue', label='Groundtruth')
plt.legend()
plt.show()


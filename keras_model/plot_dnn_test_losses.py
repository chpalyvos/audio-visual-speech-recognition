import os
import cPickle
from matplotlib import pyplot as plt
import matplotlib
import numpy as np

# Plot test error versus number of hidden layers for 300, 400, 500, 600 layer sizes
src_dir = '/media/nick/Files-Linux/articulatory-inversion-keras/articulatory_inversion_plp/grid_search_best_models'
file_name = 'test_losses_grid.pkl'
file_name2 = 'test_losses_grid2.pkl'

font = {'family': 'normal',
        'weight': 'normal',
        'size': 30}
matplotlib.rc('font', **font)

with open(os.path.join(src_dir, file_name), 'rb') as f:
    test_losses = cPickle.load(f)
with open(os.path.join(src_dir, file_name2), 'rb') as f:
    test_losses2 = cPickle.load(f)

x = [2, 3, 4, 5, 6]

err2 = test_losses[2]
err2.extend(test_losses2[2])
err3 = test_losses[3]
err3.extend(test_losses2[3])
err4 = test_losses[4]
err4.extend(test_losses2[4])
err5 = test_losses[5]
err5.extend(test_losses2[5])
err6 = test_losses[6]
err6.extend(test_losses2[6])

all_errors = np.array([err2, err3, err4, err5, err6])

plt.figure(1)
plt.ylabel('Average RMSE (mm)')
plt.xlabel('Number of hidden layers')
plt.xticks(x)

plt.plot(x, all_errors[:, 0], '-o', label='300 neurons', linewidth=3.0)
plt.plot(x, all_errors[:, 1], '-o', label='400 neurons', linewidth=3.0)
plt.plot(x, all_errors[:, 2], '-o', label='500 neurons', linewidth=3.0)
plt.plot(x, all_errors[:, 3], '-o', label='600 neurons', linewidth=3.0)
plt.plot(x, all_errors[:, 4], '-o', label='700 neurons', linewidth=3.0)
plt.plot(x, all_errors[:, 5], '-o', label='800 neurons', linewidth=3.0)
plt.plot(x, all_errors[:, 6], '-o', label='900 neurons', linewidth=3.0)
plt.plot(x, all_errors[:, 7], '-o', label='1000 neurons', linewidth=3.0)

plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=4, mode="expand", borderaxespad=0.)
plt.show()



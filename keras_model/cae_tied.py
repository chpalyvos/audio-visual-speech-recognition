import keras.backend as K
from keras.models import Model, load_model
from keras.layers import Input, Dense, Activation, Dropout, BatchNormalization
from keras import regularizers
from keras.callbacks import ModelCheckpoint, EarlyStopping
from extra_classes import DenseTransposeTied
import numpy as np
import os
from scripts.input_output.txt_io import TxtDataRead


def rmse_contractive(y_true, y_pred):
    err = K.sqrt(K.mean(K.sum(K.square(y_pred - y_true), axis=1)))  # RMSE
    if is_contractive:
        contraction = 0
        for i in range(len(hidden_layers)):
            W = K.variable(value=model.get_layer(name='hidden{}'.format(str(i))).get_weights()[0])
            W = K.transpose(W)
            h = model.get_layer(name='hidden{}'.format(str(i))).output
            dh = h*(1-h)
            contraction += contractive_lamda*K.sum(dh ** 2 * K.sum(W ** 2, axis=1), axis=1)
        W = K.variable(value=model.get_layer(name='encoded').get_weights()[0])
        W = K.transpose(W)
        h = model.get_layer(name='encoded').output
        dh = h * (1 - h)
        contraction += contractive_lamda * K.sum(dh ** 2 * K.sum(W ** 2, axis=1), axis=1)
        err += contraction
    return err


def contraction_loss(y_true, y_pred):
    contraction = 0
    if is_contractive:
        for i in range(len(hidden_layers)):
            W = K.variable(value=model.get_layer(name='hidden{}'.format(str(i))).get_weights()[0])
            W = K.transpose(W)
            h = model.get_layer(name='hidden{}'.format(str(i))).output
            dh = h * (1 - h)
            contraction += contractive_lamda * K.sum(dh ** 2 * K.sum(W ** 2, axis=1), axis=1)
        W = K.variable(value=model.get_layer(name='encoded').get_weights()[0])
        W = K.transpose(W)
        h = model.get_layer(name='encoded').output
        dh = h * (1 - h)
        contraction += contractive_lamda * K.sum(dh ** 2 * K.sum(W ** 2, axis=1), axis=1)
    return contraction


def rmse(y_true, y_pred):
    return K.sqrt(K.mean(K.sum(K.square(y_pred - y_true), axis=-1)))


feature_splicing = True
label_deltas = False
is_contractive = True

input_dimen = 351  # 360 if feature_splicing else 40
output_dimen = 12  # 36 if label_deltas else 12
hidden_layers = [400, 400]
epochs = 30
batch_size = 256
learning_rate = 0.01
optimizer = 'Adam'  # SGD, Adam, RMSProp
L2_regularization = 0.0
activation = 'sigmoid'
dropout = 0.4
contractive_lamda = 0.001

feat_dir = '/media/nick/Files-Linux/Datasets/speech_datasets/MNGU0/dnn_data'
lab_dir = feat_dir
model_dir = '/media/nick/Files-Linux/articulatory-inversion-keras/articulatory_inversion_plp'
work_dir = '/media/nick/Files-Linux/articulatory-inversion-keras/articulatory_inversion_cae'

# Load data
x_train = os.path.join(feat_dir, 'train.txt')
y_train = os.path.join(lab_dir, 'train_labels.txt')

x_valid = os.path.join(feat_dir, 'valid.txt')
y_valid = os.path.join(lab_dir, 'valid_labels.txt')

x_test = np.loadtxt(os.path.join(feat_dir, 'test.txt'), dtype='float32')
y_test = np.loadtxt(os.path.join(lab_dir, 'test_labels.txt'), dtype='float32')

reader_train = TxtDataRead(feat_file_path=x_train, lab_file_path=y_train, batch_size=batch_size)
reader_val = TxtDataRead(feat_file_path=x_valid, lab_file_path=y_valid, batch_size=batch_size)
trSteps = 762754 // batch_size
valSteps = 38958 // batch_size

model = load_model(os.path.join(model_dir, 'best' + str(hidden_layers) + '.hdf5'), custom_objects={'rmse': rmse})

tied_layer = model.get_layer(name='encoded')
model.add(DenseTransposeTied(units=hidden_layers[-1], tied_to=tied_layer, name='decoding0'))
model.add(BatchNormalization(name='decoding_batch_normalization0'))
model.add(Activation(activation=activation, name='decoding_activation0'))
model.add(Dropout(dropout, name='decoding_dropout0'))

for i in range(len(hidden_layers)-1):
    tied_layer = model.get_layer(name='hidden{}'.format(len(hidden_layers)-1-i))
    model.add(DenseTransposeTied(units=hidden_layers[-(i+2)], tied_to=tied_layer, name='decoding{}'.format(i+1)))
    model.add(BatchNormalization(name='decoding_batch_normalization{}'.format(i+1)))
    model.add(Activation(activation=activation, name='decoding_activation{}'.format(i+1)))
    model.add(Dropout(dropout, name='decoding_dropout{}'.format(i+1)))

tied_layer = model.get_layer(name='hidden0')
model.add(DenseTransposeTied(units=input_dimen, tied_to=tied_layer, name='reconstructed'))
model.add(BatchNormalization(name='decoding_batch_normalization_reconstructed'))

model.compile(optimizer=optimizer,
              loss=rmse_contractive,
              metrics=[contraction_loss, rmse])

_callbacks = []
# Checkpoint
best_model = 'best_tied' + str(hidden_layers) + '.hdf5'
checkpointer = ModelCheckpoint(filepath=os.path.join(work_dir, best_model),
                               monitor='val_loss', mode="min",
                               verbose=1, save_best_only=True,
                               save_weights_only=True)
early_stop = EarlyStopping(monitor='val_loss',
                           min_delta=0,
                           patience=5, verbose=1, mode='min')

_callbacks.append(checkpointer)
_callbacks.append(early_stop)

history = model.fit_generator(reader_train.batch_generator_cae(),
                              steps_per_epoch=trSteps,
                              validation_data=reader_val.batch_generator_cae(),
                              validation_steps=valSteps,
                              epochs=epochs,
                              verbose=2,
                              callbacks=_callbacks)

# Test
test_loss = model.evaluate(x_test, x_test, batch_size=batch_size)
print('The RMSE on the test set is: ' + str(test_loss))



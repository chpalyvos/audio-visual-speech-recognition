import keras.backend as K
from keras.models import Sequential, load_model
from keras.layers import InputLayer, Dense, Activation, Dropout, BatchNormalization
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras import regularizers
import numpy as np
import os
import cPickle


def rmse(y_true, y_pred):
    return K.sqrt(K.mean(K.sum(K.square(y_pred - y_true), axis=-1)))


def create_dnn(num_layers = 2, layer_size = 300):

    input_dim = 351
    output_dim = 12

    layer_size = layer_size
    num_layers = num_layers
    hidden_layers = [layer_size for i in range(num_layers)]

    # Keras Model
    model = Sequential()
    # Input layer
    model.add(InputLayer(batch_input_shape=(None, input_dim), name='input'))
    # Hidden layers
    for i in range(len(hidden_layers)):
        model.add(Dense(units=hidden_layers[i],
                        # kernel_initializer='he_normal',
                        kernel_regularizer=regularizers.l2(L2_regularization),
                        name='hidden{}'.format(str(i))))
        model.add(BatchNormalization())
        model.add(Activation(activation))
        model.add(Dropout(dropout))
    # Output layer
    model.add(Dense(units=output_dim, name='encoded'))
    model.add(BatchNormalization())

    model.compile(optimizer=optimizer,
                  loss=rmse)
    # Callbacks
    _callbacks = []
    # Checkpoint
    best_model = "best" + str(hidden_layers) + ".hdf5"
    checkpointer = ModelCheckpoint(filepath=os.path.join(work_dir, best_model),
                                   monitor='val_loss', mode="min",
                                   verbose=1, save_best_only=True)
    early_stop = EarlyStopping(monitor='val_loss',
                               min_delta=0,
                               patience=5, verbose=1, mode='min')
    _callbacks.append(early_stop)
    _callbacks.append(checkpointer)

    return model, _callbacks, best_model


np.random.seed(1337)  # for reproducibility

# Directories
feat_dir = '/home/nickgreek/MNGU0/dnn_data'
lab_dir = feat_dir
work_dir = '/home/nickgreek/articulatory-inversion-keras/articulatory_inversion_plp'

# Load data
x_train = np.loadtxt(os.path.join(feat_dir, 'train.txt'), dtype='float32')
y_train = np.loadtxt(os.path.join(lab_dir, 'train_labels.txt'), dtype='float32')

x_valid = np.loadtxt(os.path.join(feat_dir, 'valid.txt'), dtype='float32')
y_valid = np.loadtxt(os.path.join(lab_dir, 'valid_labels.txt'), dtype='float32')

# Load test data
x_test = np.loadtxt(os.path.join(feat_dir, 'test.txt'), dtype='float32')
y_test = np.loadtxt(os.path.join(lab_dir, 'test_labels.txt'), dtype='float32')

# Paremeters
epochs = 50
batch_size = 256
learning_rate = 0.01
optimizer = 'Adam'
L2_regularization = 0.0
activation = 'sigmoid'
dropout = 0.4

# Grid search
test_losses = {}
for num_layer in [2, 3, 4, 5, 6]:
    test_losses[num_layer] = []
    for layer_siz in [300, 400, 500, 600]:
        model, callbacks, best = create_dnn(num_layer, layer_siz)
        # Train
        try:
            history = model.fit(x=x_train,
                                y=y_train,
                                batch_size=batch_size,
                                epochs=epochs,
                                verbose=2,
                                validation_data=(x_valid, y_valid),
                                callbacks=callbacks)
            model = load_model(os.path.join(work_dir, best), custom_objects={'rmse': rmse})
            test_loss = model.evaluate(x_test, y_test, batch_size=batch_size)
            print('The RMSE on the test set is: ' + str(test_loss))
            test_losses[num_layer].append(test_loss)
        except Exception as e:
            print("There was an error!!!", e)
print test_losses
with open(os.path.join(work_dir, 'test_losses_grid.txt'),'wb') as f:
    cPickle.dump(test_losses, f)

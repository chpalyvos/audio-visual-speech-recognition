import keras.backend as K
from keras.models import Sequential, load_model
from keras.layers import InputLayer, Dense, Activation, Dropout, BatchNormalization
from keras.optimizers import SGD
from keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping
from keras import regularizers
import numpy as np
import os
from matplotlib import pyplot as plt
from scripts.input_output.txt_io import TxtDataRead


def rmse(y_true, y_pred):
    return K.sqrt(K.mean(K.sum(K.square(y_pred - y_true), axis=-1)))


input_dimen = 351
output_dimen = 12
hidden_layers = [400, 400]
epochs = 30
batch_size = 256
learning_rate = 0.01
optimizer = 'Adam'  # SGD, Adam, RMSProp
L2_regularization = 0.0
activation = 'sigmoid'
dropout = 0.4

feat_dir = '/media/nick/Files-Linux/Datasets/speech_datasets/MNGU0/dnn_data'
lab_dir = feat_dir
work_dir = '/media/nick/Files-Linux/articulatory-inversion-keras/articulatory_inversion_plp'

# Load data
# x_train = np.loadtxt(os.path.join(feat_dir, 'train.txt'), dtype='float32')
# y_train = np.loadtxt(os.path.join(lab_dir, 'train_labels.txt'), dtype='float32')
#
# x_valid = np.loadtxt(os.path.join(feat_dir, 'valid.txt'), dtype='float32')
# y_valid = np.loadtxt(os.path.join(lab_dir, 'valid_labels.txt'), dtype='float32')

# Load test data
x_test = np.loadtxt(os.path.join(feat_dir, 'test.txt'), dtype='float32')
y_test = np.loadtxt(os.path.join(lab_dir, 'test_labels.txt'), dtype='float32')

# Load best model
best_model = "best.hdf5"
model = load_model(os.path.join(work_dir, best_model), custom_objects={'rmse': rmse})
# Test
test_loss = model.evaluate(x_test, y_test, batch_size=batch_size)
print('The RMSE on the test set is: ' + str(test_loss))

# Plot trajectories on best model
reconstructed_labels = model.predict(x_test)
sample_reconstructed = []
for i in range(1000):
    sample_reconstructed.append(reconstructed_labels[i][0])
sample_ground = []
for i in range(1000):
    sample_ground.append(y_test[i][0])
plt.plot(sample_reconstructed, 'red', label='Reconstructed')
plt.plot(sample_ground, 'blue', label='Groundtruth')
plt.legend()
plt.show()

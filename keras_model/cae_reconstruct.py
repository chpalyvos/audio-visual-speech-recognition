import keras.backend as K
from keras.models import Sequential, load_model
from keras.layers import InputLayer, Dense, Activation, Dropout, BatchNormalization
from extra_classes import DenseTransposeTied
from keras import regularizers
import numpy as np
import os


def rmse_contractive(y_true, y_pred):
    err = K.sqrt(K.mean(K.sum(K.square(y_pred - y_true), axis=1)))  # RMSE
    if is_contractive:
        contraction = 0
        for i in range(len(hidden_layers)):
            W = K.variable(value=model.get_layer(name='hidden{}'.format(str(i))).get_weights()[0])
            W = K.transpose(W)
            h = model.get_layer(name='hidden{}'.format(str(i))).output
            dh = h*(1-h)
            contraction += contractive_lamda*K.sum(dh ** 2 * K.sum(W ** 2, axis=1), axis=1)
        W = K.variable(value=model.get_layer(name='encoded').get_weights()[0])
        W = K.transpose(W)
        h = model.get_layer(name='encoded').output
        dh = h * (1 - h)
        contraction += contractive_lamda * K.sum(dh ** 2 * K.sum(W ** 2, axis=1), axis=1)
        err += contraction
    return err


def contraction_loss(y_true, y_pred):
    contraction = 0
    if is_contractive:
        for i in range(len(hidden_layers)):
            W = K.variable(value=model.get_layer(name='hidden{}'.format(str(i))).get_weights()[0])
            W = K.transpose(W)
            h = model.get_layer(name='hidden{}'.format(str(i))).output
            dh = h*(1-h)
            contraction += contractive_lamda*K.sum(dh ** 2 * K.sum(W ** 2, axis=1), axis=1)
        W = K.variable(value=model.get_layer(name='encoded').get_weights()[0])
        W = K.transpose(W)
        h = model.get_layer(name='encoded').output
        dh = h * (1 - h)
        contraction += contractive_lamda * K.sum(dh ** 2 * K.sum(W ** 2, axis=1), axis=1)
    return contraction


def rmse(y_true, y_pred):
    return K.sqrt(K.mean(K.sum(K.square(y_pred - y_true), axis=-1)))


is_contractive = True
input_dimen = 351
output_dimen = 12
hidden_layers = [900, 900]
epochs = 30
batch_size = 256
learning_rate = 0.01
optimizer = 'Adam'  # SGD, Adam, RMSProp
L2_regularization = 0.0
activation = 'sigmoid'
dropout = 0.4
contractive_lamda = 0.0

feat_dir = '/media/nick/Files-Linux/Datasets/speech_datasets/MNGU0/dnn_data'
lab_dir = feat_dir
model_dir = '/media/nick/Files-Linux/articulatory-inversion-keras/articulatory_inversion_cae'
work_dir = '/media/nick/Files-Linux/Datasets/speech_datasets/MNGU0/CAE_reconstructed'
reconstruct_dir = '/media/nick/Files-Linux/Datasets/speech_datasets/MNGU0/CAE_reconstructed'

model = Sequential()
# Encoding
# Input layer
model.add(InputLayer(batch_input_shape=(None, input_dimen), name='input'))
# Hidden layers
for i in range(len(hidden_layers)):
    model.add(Dense(units=hidden_layers[i],
                    # kernel_initializer='he_normal',
                    kernel_regularizer=regularizers.l2(L2_regularization),
                    name='hidden{}'.format(str(i))))
    model.add(BatchNormalization())
    model.add(Activation(activation))
    model.add(Dropout(dropout))
# Output layer
model.add(Dense(units=output_dimen, name='encoded'))
model.add(BatchNormalization())

# Decoding
tied_layer = model.get_layer(name='encoded')
for i in range(len(hidden_layers)):
    model.add(DenseTransposeTied(units=hidden_layers[i], tied_to=tied_layer, name='decoding{}'.format(str(i))))
    model.add(BatchNormalization(name='decoding_batch_normalization{}'.format(str(i))))
    model.add(Activation(activation=activation, name='decoding_activation{}'.format(str(i))))
    model.add(Dropout(dropout, name='decoding_dropout{}'.format(str(i))))
    tied_layer = model.get_layer(name='hidden{}'.format(str(len(hidden_layers)-1-i)))

model.add(DenseTransposeTied(units=input_dimen, tied_to=tied_layer, name='reconstructed'))
model.add(BatchNormalization(name='decoding_batch_normalization_reconstructed'))

model.compile(optimizer=optimizer,
              loss=rmse_contractive,
              metrics=[contraction_loss, rmse])

# Load data
# x_train = np.loadtxt(os.path.join(feat_dir, 'xab'), dtype='float32')
# y_train = np.loadtxt(os.path.join(lab_dir, 'train_labels.txt'), dtype='float32')
#
# x_valid = np.loadtxt(os.path.join(feat_dir, 'valid.txt'), dtype='float32')
# y_valid = np.loadtxt(os.path.join(lab_dir, 'valid_labels.txt'), dtype='float32')

# Load test data
x_test = np.loadtxt(os.path.join(feat_dir, 'valid.txt'), dtype='float32')
# y_test = np.loadtxt(os.path.join(lab_dir, 'test_labels.txt'), dtype='float32')

# Load best model
if contractive_lamda != 0:
    best_model = 'best_tied_contractive' + str(hidden_layers) + '.hdf5'
else:
    best_model = 'best_tied' + str(hidden_layers) + '.hdf5'
model.load_weights(os.path.join(model_dir, best_model))
# Test
test_loss = model.evaluate(x_test, x_test, batch_size=batch_size)
print('The RMSE on the test set is: ' + str(test_loss))

# Plot trajectories on best model
reconstructed_features = model.predict(x_test)
# Cut middle frame and cut deltas
rec_1frame = reconstructed_features[:, 4*39:4*39+13]

# Save to txt
np.savetxt(os.path.join(reconstruct_dir, 'rec_valid.txt'), rec_1frame, delimiter=' ')

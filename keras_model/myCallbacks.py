from keras.callbacks import Callback


class ValHistory(Callback):
    def on_train_begin(self, logs=None):
        self.val_loss = []

    def on_epoch_end(self, epoch, logs=None):
        self.val_loss = logs.get('loss')

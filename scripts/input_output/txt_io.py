from itertools import islice
import numpy as np


class TxtDataRead(object):
    def __init__(self, feat_file_path, lab_file_path, batch_size=256):
        self.feat_file_path = feat_file_path
        self.feat_file = open(self.feat_file_path)
        self.lab_file_path = lab_file_path
        self.lab_file = open(self.lab_file_path)

        self.feat_slices = islice(self.feat_file, batch_size)
        self.lab_slices = islice(self.lab_file, batch_size)

        self.batch_size = batch_size
        self.feat_batch = None
        self.lab_batch = None

    def read_full_data_in_matrix(self):
        feat = []
        for line in self.feat_file:
            temp = line.split()
            temp = map(float, temp)
            feat.append(temp)
        lab = []
        for line in self.lab_file:
            temp = line.split()
            temp = map(float, temp)
            lab.append(temp)
        return np.array(feat), np.array(lab)

    def get_next_batch(self):
        # for features
        f = []
        self.feat_batch = list(self.feat_slices)
        for line in self.feat_batch:
            temp = line.split()
            temp = map(float, temp)
            f.append(temp)
        self.feat_slices = islice(self.feat_file, self.batch_size)
        # for labels
        l = []
        self.lab_batch = list(self.lab_slices)
        for line in self.lab_batch:
            temp = line.split()
            temp = map(float, temp)
            l.append(temp)
        self.lab_slices = islice(self.lab_file, self.batch_size)
        return np.array(f), np.array(l)

    def batch_generator(self):
        while True:
            f, l = self.get_next_batch()
            if self.feat_batch and self.lab_batch:
                yield f,l
            else:
                self.close_files()
                self.feat_file = open(self.feat_file_path)
                self.lab_file = open(self.lab_file_path)
                self.feat_slices = islice(self.feat_file, self.batch_size)
                self.lab_slices = islice(self.lab_file, self.batch_size)

    def batch_generator_cae(self):
        while True:
            f, l = self.get_next_batch()
            if self.feat_batch and self.lab_batch:
                yield f, f
            else:
                self.close_files()
                self.feat_file = open(self.feat_file_path)
                self.lab_file = open(self.lab_file_path)
                self.feat_slices = islice(self.feat_file, self.batch_size)
                self.lab_slices = islice(self.lab_file, self.batch_size)

    def close_files(self):
        self.feat_file.close()
        self.lab_file.close()

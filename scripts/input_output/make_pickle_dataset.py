import cPickle

# Enter paths
data_dir = '/home/nick/Documents/Articulatory-Inversion/CAE/MNGU0/'
out_dir = '/home/nick/Documents/Articulatory-Inversion/CAE/MNGU0/'

# Enter dataset paths: train, validation, test
train_data = data_dir+'train.txt'
train_labels = data_dir+'train_labels.txt'
valid_data = data_dir+'valid.txt'
valid_labels = data_dir+'valid_labels.txt'
test_data = data_dir+'test.txt'
test_labels = data_dir+'test_labels.txt'

out_file = out_dir+'mngu0.pkl'

data_dict = {}

def read_and_add(filename, name):
    with open(filename) as f:
        a = f.readlines()
        b = []
        i = 1
        for line in a:
            temp = line.split()
            temp = map(float, temp)
            b.append(temp)
            # print i
            i += 1
    data_dict[name] = b

if __name__ == '__main__':
    read_and_add(train_data, 'train_data')
    read_and_add(train_labels, 'train_labels')
    read_and_add(valid_data, 'valid_data')
    read_and_add(valid_labels, 'valid_labels')
    read_and_add(test_data, 'test_data')
    read_and_add(test_labels, 'test_labels')

    with open(out_file,'wb') as f:
        cPickle.dump(data_dict,out_file)

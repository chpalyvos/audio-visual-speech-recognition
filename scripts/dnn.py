import cPickle
import math

import numpy as np
import tensorflow as tf

from input_output.txt_io import TxtDataRead
from LoggingModels import My_Logger

# Enter paths
# 1-frame
data_dir = '/home/nick/Documents/Articulatory-Inversion/MNGU0_pfiles/mngu0-1frame/'
# 10-frames neighbourhood
# data_dir = '/home/nick/Documents/Articulatory-Inversion/CAE/MNGU0/'
save_dir = '/home/nick/Documents/ArticulatoryInversionProject-Output/'

# Enter dataset paths: train, validation, test
train_data = data_dir+'train_1frame.txt'
train_labels = data_dir+'train_labels_1frame.txt'
valid_data = data_dir+'valid_1frame.txt'
valid_labels = data_dir+'valid_labels_1frame.txt'
test_data = data_dir+'test_1frame.txt'
test_labels = data_dir+'test_labels_1frame.txt'
## Enter network architecture ##
# number_of_input_features, number_of_output_labels
num_input = 41
num_output = 12
# number_of_layers, hidden_units_per_layer
num_hidden_layers = 5
num_hidden_units = 64
# number_of_epochs, minibatch_size, learning rate, optimizer type, L2 regularization, momentum, etc.
num_epochs = 10
batch_size = 256
learning_rate = 0.001
L2_reg = 0
dropout = None
optimizer_type = 'Adam' # SGD, Adam, AdaGrad, RMSProp

# Enter save paths
path_to_save_weights = save_dir+'mngu0_{0}_{1}.ckpt'.format(num_hidden_units,num_hidden_layers)
path_to_save_labels = save_dir+'reconstructed_test_{0}_{1}.pkl'.format(num_hidden_units,num_hidden_layers)
# Initialize logger and log DNN architecture
logger = My_Logger(save_dir+'Logs/')
logger.log_dnn({
    'Input_Size': num_input,
    'Output_Size': num_output,
    'Hidden_Layers': num_hidden_layers,
    'Hidden_Units_per_layer': num_hidden_units,
    'Epochs': num_epochs,
    'Batch_size': batch_size,
    'Learning_Rate': learning_rate,
    'Optimizer': optimizer_type,
    'L2_Regularization': L2_reg,
    'Dropout': dropout
})

with tf.Graph().as_default() as g:
    with tf.device('/gpu:0'):
        # DNN
        # Define data
        features = tf.placeholder(dtype=tf.float32, shape=(None, num_input))
        labels = tf.placeholder(dtype=tf.float32, shape=(None, num_output))

        # Define layer names and neurons
        sigmoid_layer_names = ["hidden{}".format(s+1) for s in xrange(num_hidden_layers)]
        layer_units = [num_input]
        layer_units.extend([num_hidden_units]*num_hidden_layers)
        layer_units.append(num_output)
        print(layer_units)
        # Define all layers with for loops
        sigmoid_layers = []
        weights = []
        biases = []
        for i in xrange(num_hidden_layers):
            with tf.name_scope(sigmoid_layer_names[i]):
                weights.append(
                    tf.Variable(
                        tf.truncated_normal(
                            shape=[layer_units[i], layer_units[i+1]],
                            stddev=1.0 / math.sqrt(float(layer_units[i])),
                            name='Weights{0}'.format(i+1)
                        )
                    )
                )
                biases.append(
                    tf.Variable(
                        tf.zeros(
                            shape=[layer_units[i+1]],
                            name='Biases{0}'.format(i+1)
                        )
                    )
                )
                if i == 0:
                    # First layer
                    sigmoid_layers.append(
                        tf.nn.sigmoid(tf.matmul(features, weights[i]) + biases[i])
                    )
                else:
                    # Hidden layers
                    sigmoid_layers.append(
                        tf.nn.sigmoid(tf.matmul(sigmoid_layers[i-1], weights[i]) + biases[i])
                    )
        # Linear Regression layer
        with tf.name_scope('Linear_Regression_Layer'):
            weights.append(
                tf.Variable(
                    tf.truncated_normal(
                        shape=[layer_units[-2], num_output],
                        stddev=1.0/math.sqrt(float(layer_units[-2])),
                        name='Weights{0}'.format(num_hidden_layers+1)
                    )
                )
            )
            biases.append(
                tf.Variable(
                    tf.zeros(
                        shape=[num_output],
                        name='Biases{0}'.format(num_hidden_layers+1)
                    )
                )
            )
        logits = tf.matmul(sigmoid_layers[-1], weights[-1]) + biases[-1]

        # Define train loss function
        # Sum of squares
        temp = tf.reduce_sum(tf.square(logits - labels),axis=1)
        # L2 Regularization
        regularizer = 0
        for weight in weights:
            regularizer += tf.nn.l2_loss(weight)
        # Total train loss function (RMSE)
        train_loss = tf.sqrt(tf.reduce_mean(0.5*temp + L2_reg*regularizer),name='Train_RMSE')

        # Define training op: Gradient Descent, RMSProp, AdaGrad, AdamOptimizer
        optimizer = None
        if optimizer_type == 'SGD':
            optimizer = tf.train.GradientDescentOptimizer(learning_rate)
        elif optimizer_type == 'Adam':
            optimizer = tf.train.AdamOptimizer(learning_rate)
        global_step = tf.Variable(0, name='global_step', trainable=False)
        train_op = optimizer.minimize(train_loss, global_step=global_step)

        # Define validation loss function
        validation_loss = tf.sqrt(
            tf.reduce_mean(
                0.5*tf.reduce_sum(
                    tf.square(logits - labels),
                    axis=1
                )
            ),
            name='Validation_RMSE'
        )

    # Initialize
    init = tf.global_variables_initializer()
    config = tf.ConfigProto(allow_soft_placement=True)
    sess = tf.Session(config=config)
    sess.run(init)

    # Train, print results per epoch
    print('--Training and validating model...')
    for epoch in range(num_epochs):
        # Train error
        per_batch_cost = []
        reader_train = TxtDataRead(feat_file_path=train_data, lab_file_path=train_labels, batch_size=batch_size)
        for f, l in reader_train.batch_generator():
            _, loss_val = sess.run(
                [train_op, train_loss],
                feed_dict={
                    features: f,
                    labels: l
                })
            per_batch_cost.append(loss_val)
        reader_train.close_files()
        total_cost = np.mean(per_batch_cost)
        # Validation error
        per_batch_validation_cost = []
        reader_val = TxtDataRead(feat_file_path=valid_data, lab_file_path=valid_labels)
        for vf, vl in reader_val.batch_generator():
            validation_loss_val = sess.run(
                validation_loss,
                feed_dict={
                    features: vf,
                    labels: vl
                })
            per_batch_validation_cost.append(validation_loss_val)
        reader_val.close_files()
        total_validation_cost = np.mean(per_batch_validation_cost)
        # Print results
        logger.log_file('Epoch {0}: Training error: {1}, Validation error: {2}'.format(epoch, total_cost, total_validation_cost))
    # Test error
    print('--Testing model...')
    test_error = []
    reader_test = TxtDataRead(feat_file_path=test_data, lab_file_path=test_labels)
    for f, l in reader_test.batch_generator():
        test_loss_val = sess.run(
            validation_loss,
            feed_dict={
                features: f,
                labels: l
            })
        test_error.append(test_loss_val)
    reader_test.close_files()
    total_test_error = np.mean(test_error)
    logger.log_file('Test error: {0}'.format(total_test_error))

    # Save trained weights-biases, reconstructed articulatory features
    print('--Extracting reconstructed labels...')
    reader_test = TxtDataRead(feat_file_path=test_data, lab_file_path=test_labels)
    f, l = reader_test.read_full_data_in_matrix()
    reconstructed = sess.run(
        logits,
        feed_dict={
            features: f,
            labels: l
        })
    reader_test.close_files()
    print('--Saving reconstructed labels...')
    fsave = open(path_to_save_labels,'wb')
    cPickle.dump(reconstructed, fsave)
    fsave.close()
    logger.log_file('----Reconstructed labels saved at file: '+path_to_save_labels)
    print('--Saving model parameters (weights,biases)...')
    saver = tf.train.Saver(
            {
                'Weights{0}'.format(i+1): weights[i] for i in xrange(num_hidden_layers+1)
            }.update({
                'Biases{0}'.format(i+1): biases[i] for i in xrange(num_hidden_layers+1)
            })
    )
    save_path = saver.save(sess,path_to_save_weights)
    logger.log_file('----Model saved in file {0}'.format(save_path))

# Close logger
logger.log_file('\n----------------END--------------\n\n')
logger.log_close()

import tensorflow as tf
import numpy as np
from input_output.txt_io import TxtDataRead
from scripts.LoggingModels import My_Logger

# Enter paths
# 1-frame
data_dir = '/home/nick/Documents/Articulatory-Inversion/MNGU0_pfiles/mngu0-1frame/'
# 10-frames neighbourhood
# data_dir = '/home/nick/Documents/Articulatory-Inversion/CAE/MNGU0/'
restore_dir = '/home/nick/Documents/ArticulatoryInversionProject-Output/'
save_dir = '/home/nick/Documents/ArticulatoryInversionProject-Output/CAE-Output/'

# Enter dataset paths: train, validation, test
train_data = data_dir+'train_1frame.txt'
train_labels = data_dir+'train_labels_1frame.txt'
valid_data = data_dir+'valid_1frame.txt'
valid_labels = data_dir+'valid_labels_1frame.txt'
test_data = data_dir+'test_1frame.txt'
test_labels = data_dir+'test_labels_1frame.txt'
## Enter network architecture ##
# number_of_input_features, number_of_output_labels
num_input = 41
num_output = 12
# number_of_layers, hidden_units_per_layer
num_hidden_layers = 5
num_hidden_units = 64
# number_of_epochs, minibatch_size, learning rate, optimizer type, L2 regularization, momentum, etc.
num_epochs = 10
batch_size = 256
learning_rate = 0.001
L2_reg = 0
dropout = None
optimizer_type = 'Adam' # SGD, Adam, AdaGrad, RMSProp
# Specific for AutoEncoder
tied_weights = True
contraction_level = 0

# Enter save paths
saved_weights = restore_dir + 'mngu0_{0}_{1}.ckpt'.format(num_hidden_units,num_hidden_layers)

# Initialize logger and log DNN architecture
logger = My_Logger(save_dir+'Logs')
logger.log_dnn({
    'Input_Size': num_input,
    'Output_Size': num_output,
    'Hidden_Layers': num_hidden_layers,
    'Hidden_Units_per_layer': num_hidden_units,
    'Epochs': num_epochs,
    'Batch_size': batch_size,
    'Learning_Rate': learning_rate,
    'Optimizer': optimizer_type,
    'L2_Regularization': L2_reg,
    'Dropout': dropout
})

with tf.Graph().as_default() as g:
    with tf.device('/gpu:0'):
        # CAE
        # Define input data
        features = tf.placeholder(dtype=tf.float32, shape=(None, num_input))

        # Define ENCODING layer names and neurons
        encoding_layer_names = ["hidden{}".format(s + 1) for s in xrange(num_hidden_layers)]
        encoding_layer_names.extend(['Linear_Regression_Layer'])
        encoding_layer_units = [num_input]
        encoding_layer_units.extend([num_hidden_units]*num_hidden_layers)
        encoding_layer_units.append(num_output)
        print(encoding_layer_names)
        print(encoding_layer_units)
        # Define DECODING layer names and neurons
        decoding_layer_names = ["decoding{}".format(s + 1) for s in xrange(num_hidden_layers+1)]
        decoding_layer_units = [num_output]
        decoding_layer_units.extend([num_hidden_units]*num_hidden_layers)
        decoding_layer_units.extend([num_input])
        print(decoding_layer_names)
        print(decoding_layer_units)

        # Define all layers with for loops
        sigmoid_layers = []
        weights = []
        biases = []
        for i in xrange(num_hidden_layers):
            with tf.name_scope(encoding_layer_names[i]):
                weights.append(
                    tf.Variable(
                        tf.zeros(
                            shape=[encoding_layer_units[i], encoding_layer_units[i + 1]],
                            name='Weights{0}'.format(i + 1)
                        )
                    )
                )
                biases.append(
                    tf.Variable(
                        tf.zeros(
                            shape=[encoding_layer_units[i + 1]],
                            name='Biases{0}'.format(i + 1)
                        )
                    )
                )
                if i == 0:
                    # First layer
                    sigmoid_layers.append(
                        tf.nn.sigmoid(tf.matmul(features, weights[i]) + biases[i])
                    )
                else:
                    # Hidden layers
                    sigmoid_layers.append(
                        tf.nn.sigmoid(tf.matmul(sigmoid_layers[i - 1], weights[i]) + biases[i])
                    )
        with tf.name_scope(encoding_layer_names[-1]):
            # Linear Regression Layer
            weights.append(
                tf.Variable(
                    tf.zeros(
                        shape=[encoding_layer_units[-2], num_output],
                        name='Weights{0}'.format(num_hidden_layers+1)
                    )
                )
            )
            biases.append(
                tf.Variable(
                    tf.zeros(
                        shape=[num_output],
                        name='Biases{0}'.format(num_hidden_layers+1)
                    )
                )
            )
        sigmoid_layers.append(tf.matmul(sigmoid_layers[-1], weights[-1]) + biases[-1])
        saver = tf.train.Saver(
            {
                    'Weights{0}'.format(i + 1): weights[i] for i in xrange(num_hidden_layers + 1)
                }.update({
                    'Biases{0}'.format(i + 1): biases[i] for i in xrange(num_hidden_layers + 1)
                })
        )

        decoding_layers = []
        decoding_weights = []
        decoding_biases = []
        for i in xrange(num_hidden_layers+1):
            with tf.name_scope(decoding_layer_names[i]):
                decoding_weights.append(
                    tf.Variable(
                        tf.zeros(
                            shape=[decoding_layer_units[i], decoding_layer_units[i + 1]],
                            name='Decoding_Weights{0}'.format(i + 1)
                        )
                    )
                )
                decoding_biases.append(
                    tf.Variable(
                        tf.zeros(
                            shape=[decoding_layer_units[i + 1]],
                            name='Decoding_Biases{0}'.format(i + 1)
                        )
                    )
                )
                if i == 0:
                    # First layer
                    decoding_layers.append(
                        tf.nn.sigmoid(tf.matmul(sigmoid_layers[-1], decoding_weights[i]) + decoding_biases[i])
                    )
                else:
                    # Hidden layers
                    decoding_layers.append(
                        tf.nn.sigmoid(tf.matmul(decoding_layers[i - 1], decoding_weights[i]) + decoding_biases[i])
                    )
        # # Linear Regression layer
        # with tf.name_scope('Linear_Regression_Layer_Final'):
        #     decoding_weights.append(
        #         tf.Variable(
        #             tf.zeros(
        #                 shape=[decoding_layer_units[-2], num_input],
        #                 name='Decoding_Weights{0}'.format(num_hidden_layers + 1)
        #             )
        #         )
        #     )
        #     decoding_biases.append(
        #         tf.Variable(
        #             tf.zeros(
        #                 shape=[num_input],
        #                 name='Decoding_Biases{0}'.format(num_hidden_layers + 1)
        #             )
        #         )
        #     )
        hidden_representation = sigmoid_layers[-1]
        logits = decoding_layers[-1] # tf.matmul(decoding_layers[-1], decoding_weights[-1]) + decoding_biases[-1]

        # Frobenius Norm Regularization
        grad = tf.gradients(ys=hidden_representation, xs=features)
        frob = tf.norm(grad)/batch_size
        # Loss function
        temp = tf.reduce_sum(tf.square(logits - features), axis=1)
        train_loss = tf.sqrt(tf.reduce_mean(0.5*temp) + contraction_level*frob, name='Train_RMSE')
        # Define training op: Gradient Descent, RMSProp, AdaGrad, AdamOptimizer
        optimizer = None
        if optimizer_type == 'SGD':
            optimizer = tf.train.GradientDescentOptimizer(learning_rate)
        elif optimizer_type == 'Adam':
            optimizer = tf.train.AdamOptimizer(learning_rate)
        global_step = tf.Variable(0, name='global_step', trainable=False)
        train_op = optimizer.minimize(train_loss, global_step=global_step)

        # Validation loss
        # Define validation loss function
        validation_loss = tf.sqrt(
            tf.reduce_mean(
                0.5*tf.reduce_sum(
                    tf.square(logits - features),
                    axis=1
                )
            ),
            name='Validation_RMSE'
        )

    config = tf.ConfigProto(allow_soft_placement=True)
    sess = tf.Session(config=config)
    # Initialize
    init = tf.global_variables_initializer()
    sess.run(init)
    # Initialize encoding and decoding weights-biases
    saver.restore(sess, saved_weights)
    for i in xrange(num_hidden_layers+1):
        decoding_weights[i] = tf.transpose(weights[-(i+1)])
        decoding_biases[i] = tf.transpose(biases[-(i+1)])

    # Train, print results per epoch
    print('--Training and validating model...')
    for epoch in range(num_epochs):
        # Train error
        per_batch_cost = []
        reader_train = TxtDataRead(feat_file_path=train_data, lab_file_path=train_labels, batch_size=batch_size)
        for f, l in reader_train.batch_generator():
            _, loss_val = sess.run(
                [train_op, train_loss],
                feed_dict={
                    features: f
                })
            per_batch_cost.append(loss_val)
        reader_train.close_files()
        total_cost = np.mean(per_batch_cost)
        # Validation error
        per_batch_validation_cost = []
        reader_val = TxtDataRead(feat_file_path=valid_data, lab_file_path=valid_labels)
        for vf, vl in reader_val.batch_generator():
            validation_loss_val = sess.run(
                validation_loss,
                feed_dict={
                    features: vf
                })
            per_batch_validation_cost.append(validation_loss_val)
        reader_val.close_files()
        total_validation_cost = np.mean(per_batch_validation_cost)
        # Print results
        logger.log_file('Epoch {0}: Training error: {1}, Validation error: {2}'.format(epoch, total_cost,
                                                                                       total_validation_cost))
import tensorflow as tf
import math

num_input = 410
num_output = 12
# number_of_layers, hidden_units_per_layer
num_hidden_layers = 3
num_hidden_units = 300
# number_of_epochs, minibatch_size, learning rate, optimizer type, L2 regularization, momentum, etc.
num_epochs = 10
batch_size = 256
learning_rate = 0.001
L2_reg = 0.01
dropout = None
optimizer_type = 'SGD' # SGD, Adam, AdaGrad, RMSProp

save_dir = '/home/nick/Documents/ArticulatoryInversionProject-Output/'
path_to_save_weights = save_dir+'mngu0_{0}_{1}.ckpt'.format(num_hidden_units,num_hidden_layers)

with tf.Graph().as_default() as g:
    # Define layer names and neurons
    sigmoid_layer_names = ["hidden{}".format(s + 1) for s in xrange(num_hidden_layers)]
    layer_units = [num_input]
    layer_units.extend([num_hidden_units]*num_hidden_layers)
    layer_units.append(num_output)
    # Define all layers with for loops
    sigmoid_layers = []
    weights = []
    biases = []
    for i in xrange(num_hidden_layers):
        with tf.name_scope(sigmoid_layer_names[i]):
            weights.append(
                tf.Variable(
                    tf.truncated_normal(
                        shape=[layer_units[i], layer_units[i + 1]],
                        stddev=1.0/math.sqrt(float(layer_units[i])),
                        name='Weights{0}'.format(i + 1)
                    )
                )
            )
            biases.append(
                tf.Variable(
                    tf.zeros(
                        shape=[layer_units[i + 1]],
                        name='Biases{0}'.format(i + 1)
                    )
                )
            )
            # if i == 0:
            #     # First layer
            #     sigmoid_layers.append(
            #         tf.nn.sigmoid(tf.matmul(features, weights[i]) + biases[i])
            #     )
            # else:
            #     # Hidden layers
            #     sigmoid_layers.append(
            #         tf.nn.sigmoid(tf.matmul(sigmoid_layers[i - 1], weights[i]) + biases[i])
            #     )
    # Linear Regression layer
    with tf.name_scope('Linear_Regression_Layer'):
        weights.append(
            tf.Variable(
                tf.truncated_normal(
                    shape=[layer_units[-2], num_output],
                    stddev=1.0/math.sqrt(float(layer_units[-2])),
                    name='Weights{0}'.format(num_hidden_layers + 1)
                )
            )
        )
        biases.append(
            tf.Variable(
                tf.zeros(
                    shape=[num_output],
                    name='Biases{0}'.format(num_hidden_layers + 1)
                )
            )
        )
        # logits = tf.matmul(sigmoid_layers[-1], weights[-1]) + biases[-1]

        saver = tf.train.Saver(
            {
                'Weights{0}'.format(i + 1): weights[i] for i in xrange(num_hidden_layers + 1)
                }.update({
                 'Biases{0}'.format(i + 1): biases[i] for i in xrange(num_hidden_layers + 1)
             })
        )

    a = tf.Variable(
        tf.truncated_normal(
            shape=[layer_units[0], layer_units[1]],
            stddev=1.0/math.sqrt(float(layer_units[0])),
            name='testVar'
        )
    )


    with tf.Session() as sess:
        # Initialize
        init = tf.global_variables_initializer()
        sess.run(init)
        saver.restore(sess,path_to_save_weights)
        print(sess.run(weights[0]))
        print('asdadasdas')
        print(sess.run(a))

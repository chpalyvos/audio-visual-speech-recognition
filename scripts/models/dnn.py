import tensorflow as tf
import numpy as np
import cPickle
from math import sqrt

from scripts.LoggingModels import My_Logger

dnn_dict = {
    'DNN_Architecture': [410, 300, 300, 12],
    'Epochs': 20,
    'Batch_size': 256,
    'Learning_Rate': 0.01,
    'Optimizer': 'SGD', # SGD, Adam, AdaGrad, RMSProp
    'L2_Regularization': 0.0001,
    'Dropout': None
}

params_dict = {
    'Data_Directory': '/home/nick/Documents/Articulatory-Inversion/CAE/MNGU0/',
    'Output_Directory': '/home/nick/Documents/ArticulatoryInversionProject-Output/'
}

# Load DNN architecture
layer_units = dnn_dict['DNN_Architecture']
num_epochs = dnn_dict['Epochs']
batch_size = dnn_dict['Batch_size']
learning_rate = dnn_dict['Learning_Rate']
L2_reg = dnn_dict['L2_Regularization']
dropout = dnn_dict['Dropout']
optimizer_type = dnn_dict['Optimizer']

num_input = layer_units[0]
num_output = layer_units[-1]
num_hidden_layers = len(layer_units) - 2
num_hiden_units = layer_units[1:-1]
sigmoid_layer_names = ["hidden{}".format(s + 1) for s in xrange(num_hidden_layers)]

## Load extra parameters (paths etc.)
data_dir = params_dict['Data_Directory']
save_dir = params_dict['Output_Directory']
log_dir = save_dir + 'Logs/'

# Enter save paths
path_to_save_weights = save_dir+'mngu0_{0}.ckpt'.format(layer_units)
path_to_save_labels = save_dir+'mngu0_reconstructed_test_{0}.pkl'.format(layer_units)


def read_pickle_full(input_file):
    f = open(input_file,'rb')
    data_dict = cPickle.load(f)
    train_d = data_dict['train_data']
    train_l = data_dict['train_labels']
    valid_d = data_dict['valid_data']
    valid_l = data_dict['valid_labels']
    test_d = data_dict['test_data']
    test_l = data_dict['test_labels']
    return train_d, train_l, valid_d, valid_l, test_d, test_l


def batching(data, batch_size):
    n=int(len(data)/batch_size) # how many batches except the last
    last=len(data)%batch_size  # the last
    batches=[]
    for i in range(n):
        batch=data[i*batch_size:(i+1)*batch_size]
        batches.append(batch)
    batches.append(data[len(data)-last:])
    return batches


def make_placeholder_inputs():
    # Define data
    features = tf.placeholder(dtype=tf.float32, shape=(None, num_input))
    labels = tf.placeholder(dtype=tf.float32, shape=(None, num_output))
    return features, labels


def forward_computation():
    # Define all layers with for loops
    sigmoid_layers = []
    weights = []
    biases = []
    for i in xrange(num_hidden_layers):
        with tf.name_scope(sigmoid_layer_names[i]):
            weights.append(
                tf.Variable(
                    tf.truncated_normal(
                        shape=[layer_units[i], layer_units[i + 1]],
                        stddev=1.0/sqrt(float(layer_units[i])),
                        name='Weights{0}'.format(i + 1)
                    )
                )
            )
            biases.append(
                tf.Variable(
                    tf.zeros(
                        shape=[layer_units[i + 1]],
                        name='Biases{0}'.format(i + 1)
                    )
                )
            )
            if i == 0:
                # First layer
                sigmoid_layers.append(
                    tf.nn.sigmoid(tf.matmul(features, weights[i]) + biases[i])
                )
            else:
                # Hidden layers
                sigmoid_layers.append(
                    tf.nn.sigmoid(tf.matmul(sigmoid_layers[i - 1], weights[i]) + biases[i])
                )
    # Linear Regression layer
    with tf.name_scope('Linear_Regression_Layer'):
        weights.append(
            tf.Variable(
                tf.truncated_normal(
                    shape=[layer_units[-2], num_output],
                    stddev=1.0/sqrt(float(layer_units[-2])),
                    name='Weights{0}'.format(num_hidden_layers + 1)
                )
            )
        )
        biases.append(
            tf.Variable(
                tf.zeros(
                    shape=[num_output],
                    name='Biases{0}'.format(num_hidden_layers + 1)
                )
            )
        )
        logits = tf.matmul(sigmoid_layers[-1], weights[-1]) + biases[-1]
    return logits, weights, biases


def train_loss_function():
    # Define train loss function
    # Sum of squares
    temp = tf.reduce_sum(tf.square(logits - labels), axis=1)
    # L2 Regularization
    regularizer = 0
    for weight in weights:
        regularizer += tf.nn.l2_loss(weight)
    # Total train loss function (RMSE)
    train_loss_fun = tf.sqrt(tf.reduce_mean(0.5*temp + L2_reg*regularizer), name='Train_RMSE')
    return train_loss_fun


def train_operation(loss_train):
    # Define training op: Gradient Descent, RMSProp, AdaGrad, AdamOptimizer
    optimizer = None
    if optimizer_type == 'SGD':
        optimizer = tf.train.GradientDescentOptimizer(learning_rate)
    elif optimizer_type == 'Adam':
        optimizer = tf.train.AdamOptimizer(learning_rate)
    global_step = tf.Variable(0, name='global_step', trainable=False)
    train_oper = optimizer.minimize(loss_train, global_step=global_step)
    return train_oper


def rmse_loss_function():
    # Define validation loss function
    rmse_loss = tf.sqrt(
        tf.reduce_mean(
            tf.reduce_sum(
                tf.square(logits - labels),
                axis=1
            )
        ),
        name='Validation_RMSE'
    )
    return rmse_loss


def train_with_validation():
    # Train, print results per epoch
    print('--Training and validating model...')
    for epoch in range(num_epochs):
        # Train error
        per_batch_cost = []
        train_feat = batching(train_data,batch_size)
        train_lab = batching(train_labels, batch_size)
        for f, l in zip(train_feat, train_lab):
            _, loss_val = sess.run(
                [train_op, train_loss],
                feed_dict={
                    features: f,
                    labels: l
                })
            per_batch_cost.append(loss_val)
        total_cost = np.mean(per_batch_cost)
        # Validation error
        per_batch_validation_cost = []
        valid_feat = batching(valid_data, batch_size)
        valid_lab = batching(valid_labels, batch_size)
        validation_loss = rmse_loss_function()
        for vf, vl in zip(valid_feat, valid_lab):
            validation_loss_val = sess.run(
                validation_loss,
                feed_dict={
                    features: vf,
                    labels: vl
                })
            per_batch_validation_cost.append(validation_loss_val)
        total_validation_cost = np.mean(per_batch_validation_cost)
        # Print results
        logger.log_file(
            'Epoch {0}: Training error: {1}, Validation error: {2}'.format(epoch, total_cost, total_validation_cost))


def test_model():
    # Test error
    print('--Testing model...')
    test_error = []
    test_feat = batching(test_data, batch_size)
    test_lab = batching(test_labels, batch_size)
    test_loss = rmse_loss_function()
    for f, l in zip(test_feat, test_lab):
        test_loss_val = sess.run(
            test_loss,
            feed_dict={
                features: f,
                labels: l
            })
        test_error.append(test_loss_val)
    total_test_error = np.mean(test_error)
    logger.log_file('Test error: {0}'.format(total_test_error))


def save_reconstructed_labels():
    # Save trained weights-biases, reconstructed articulatory features
    print('--Extracting reconstructed labels...')
    reconstructed = sess.run(
        logits,
        feed_dict={
            features: test_data,
            labels: test_labels
        })
    print('--Saving reconstructed labels...')
    fsave = open(path_to_save_labels, 'wb')
    cPickle.dump(reconstructed, fsave)
    fsave.close()
    logger.log_file('----Reconstructed labels saved at file: ' + path_to_save_labels)


def save_checkpoint():
    print('--Saving model parameters (weights,biases)...')
    saver = tf.train.Saver(
            {
                'Weights{0}'.format(i + 1): weights[i] for i in xrange(num_hidden_layers + 1)
            }.update({
                'Biases{0}'.format(i + 1): biases[i] for i in xrange(num_hidden_layers + 1)
            })
    )
    save_path = saver.save(sess, path_to_save_weights)
    logger.log_file('----Model saved in file {0}'.format(save_path))

if __name__ == '__main__':
    # Initialize Logger and log Architecture
    logger = My_Logger(logdir=log_dir)
    logger.log_dnn(dnn_dict)

    # DNN
    with tf.Graph().as_default() as g:
        # Inputs
        features, labels = make_placeholder_inputs()
        # Forward Computation: Calculate weights, biases and logits
        logits, weights, biases = forward_computation()
        # Find train loss function
        train_loss = train_loss_function()
        train_op = train_operation(train_loss)

        # Initialize
        init = tf.global_variables_initializer()
        sess = tf.Session()
        sess.run(init)

        # Read dataset
        train_data, train_labels, valid_data, valid_labels, \
        test_data, test_labels = read_pickle_full(data_dir+'mngu0.pkl')

        # Train with validation
        train_with_validation()
        test_model()
        save_reconstructed_labels()
        save_checkpoint()

    # Close logger
    logger.log_file('\n----------------END--------------\n\n')
    logger.log_close()
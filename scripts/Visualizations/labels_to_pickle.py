import cPickle

labels_dir = '/home/nick/Documents/Articulatory-Inversion/CAE/MNGU0/'

lab = []
with open(labels_dir+'test_labels.txt','r') as f:
    for line in f:
    	temp = line.split()
    	temp = map(float,temp)
        lab.append(temp)

f = open('groundtruth_articulatory_test_labels.pkl','wb')
cPickle.dump(lab,f)

import cPickle
import numpy as np
import matplotlib.pyplot as plt

# Load groundtruth articulatory features from .pkl file (test files) Load reconstructed articulatory features from
# .pkl file Visualize combined with groundtruth in same plot (e.g. 1000 of them and for different articulators just
# to be sure it works)

reconstructed_dir = '/media/nick/Files-Linux/articulatory-inversion-keras/feature_type-plp-feature_splicing-9' \
                    '-label_option-normalized-label_deltas-False-feature_deltas-False-feature_frame-multiframe-/'
reconstructed = reconstructed_dir + 'reconstructed_labels.pkl'

with open(reconstructed, 'rb') as f:
    feat = cPickle.load(f)
with open('groundtruth_articulatory_test_labels.pkl') as f:
    ground = cPickle.load(f)

sample_reconstructed = []
for i in range(1000):
    sample_reconstructed.append(feat[i][0])
sample_ground = []
for i in range(1000):
    sample_ground.append(ground[i][0])

# print sample_reconstructed[1]
# print sample_ground[1]

# n = len(sample_reconstructed)
plt.plot(sample_reconstructed, 'red')
plt.hold(True)
plt.plot(sample_ground, 'blue')
plt.show()

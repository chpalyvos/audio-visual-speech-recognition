import matplotlib.pyplot as plt


def articulatory_file_to_table(filename):
    lab = []
    with open(filename, 'r') as f:
        for line in f:
            temp = line.split()
            temp = map(float, temp)
            lab.append(temp)
    return lab


def get_articulator_xy_values(articulator, labels):
    artx = []
    arty = []
    for i in range(len(labels)):
        artx.append(labels[i][2 * articulator])
        arty.append(labels[i][2 * articulator + 1])
    return artx, arty


data_dir = '/media/nick/Files-Linux/articulatory-inversion-keras/feature_type-plp-feature_splicing-9-label_option' \
           '-normalized-label_deltas-False-feature_deltas-False-feature_frame-multiframe-/'

test_labels = data_dir + 'reconstructed_labels.txt'

lab = articulatory_file_to_table(test_labels)
fig = plt.figure()
ax = fig.add_subplot(111)
for i in range(6):
    artx, arty = get_articulator_xy_values(i, lab)
    ax.scatter(artx, arty)
plt.show()

import time
import os
import errno


class My_Logger:

    def __init__(self, logdir):
        filename = logdir + 'DNN_Output_' + time.strftime("%d-%m-%Y") + '.txt'
        if not os.path.exists(os.path.dirname(filename)):
            try:
                os.makedirs(os.path.dirname(filename))
            except OSError as exc:  # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise
        self.f = open(filename, 'a+')

    def log_file(self, string):
        print(string)
        self.f.write(string+'\n')

    def log_dnn(self,dnn_dict):
        self.log_file('----DNN Architecture----')
        for key, value in dnn_dict.iteritems():
            if value is None:
                self.log_file('{0}: None'.format(key))
            else:
                self.log_file('{0}: {1}'.format(key,value))
        self.log_file('')

    def log_close(self):
        self.f.close()

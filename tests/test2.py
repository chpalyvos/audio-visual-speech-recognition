# import cPickle
#
# out_file = '/home/nick/Documents/Articulatory-Inversion/MNGU0_pfiles/mngu0_pickles/train.pkl'
#
# f = open(out_file)
# a = cPickle.load(f)
# print a.shape
num_hidden_layers = 2
a = {'Weights{0}'.format(i+1): 'Weights{0}'.format(i+1) for i in xrange(num_hidden_layers+1)}
b = {'Biases{0}'.format(i+1): 'Biases{0}'.format(i+1) for i in xrange(num_hidden_layers+1)}
a.update(b)
print a